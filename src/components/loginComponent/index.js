import React from 'react';
import './login-component.css';


class Logins extends React.Component {

    render() {
        return (
            <div>
                < this.loginAttr lb_name = "username" placeholder_name = "username" />
                < this.loginAttr lb_name = "password" placeholder_name = "password" />
            </div>
        )
    }

    loginAttr = (props) => {
        return (
            <div className="form-group">
                <label>{ props.lb_name }:</label>
                <input className="form-control" type="text" placeholder={props.placeholder_name }/>
            </div>
        )
    }
}
export default Logins;