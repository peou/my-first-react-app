import React from 'react';
import './ToDo.Module.css';


class Logins extends React.Component {

    render() {
        return (
            <div className='login-dv'>
                < this.tableAttr/>
            </div>
        )
    }

    todoList = () => {
        const todo_list = [
            {
                "id": 1,
                "todo" : "work",
                "time" : "8:00",
                "status" : {
                    "class" : "btn btn-info",
                    'title' : "progress"
                }
            },
            {
                "id": 2,
                "todo" : "study",
                "time" : "8:00",
                "status" : {
                    "class" : "btn btn-primary",
                    'title' : "not start"
                }
            },
            {
                "id": 3,
                "todo" : "breakfast",
                "time" : "8:00",
                "status" : {
                    "class" : "btn btn-success",
                    'title' : "done"
                }
            }
        ];
        return todo_list;
    }

    tableAttr = (props) => {
        console.log();
        return (
            <div className="table d-flex justify-content-center">
                <table>
                    < this.tableHeader />
                    {
                        this.todoList().map( item => {
                            return < this.tableBody no={item.id} todo={item.todo} time={item.time} status={item.status} />
                        })
                    }
                </table>
            </div>
        )
    }

    tableHeader = (props) => {
        const table_header_list = Object.keys(this.todoList()[0]);
        return (
            <tr>
                {
                    table_header_list.map( item => {
                        return <th> {item} </th>
                    })
                }
            </tr>
        )
    }

    tableBody = (props) => {
        return (
            <tr>
                <td> {props.no} </td>
                <td> {props.todo} </td>
                <td> {props.time} </td>
                <td>
                    <button type='button' className={props.status.class}>
                        {props.status.title}
                    </button>
                </td>
            </tr>
        )
    }
}
export default Logins;