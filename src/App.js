import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { useState } from "react";
import LoginsComponent from "./components/loginComponent";
import TodoListComponent from "./components/todoComponent";
import { Task } from "./Task";

function App() {
    const [todoList, setTodoList] = useState([]);
    const [newTask, setNewTask] = useState("");
  
    const handleChange = (event) => {
      setNewTask(event.target.value);
    };
  
    const addTask = () => {
      const task = {
        id: todoList.length === 0 ? 1 : todoList[todoList.length - 1].id + 1,
        taskName: newTask,
        completed: false,
      };
      setTodoList(task.taskName !== "" ? [...todoList, task] : todoList);
    };
  
    const deleteTask = (id) => {
      setTodoList(todoList.filter((task) => task.id !== id));
    };
  
    const completeTask = (id) => {
      setTodoList(
        todoList.map((task) => {
          if (task.id === id) {
            return { ...task, completed: true };
          } else {
            return task;
          }
        })
      );
    };
  
    return (
      <div className="App">
        <div className="addTask">
          <input onChange={handleChange} />
          <button onClick={addTask}> Add Task</button>
        </div>
        <div className="list">
          {todoList.map((task) => {
            return (
              <Task
                taskName={task.taskName}
                id={task.id}
                completed={task.completed}
                deleteTask={deleteTask}
                completeTask={completeTask}
              />
            );
          })}
        </div>
      </div>
    );
    
    // const [showText, setShowText] = useState(true);

    // const toggle_text = () => {
    //     setShowText(!showText);
    // }

    // return (
        // <div className="login-container container mt-5" style={{ width: "30%" }}>
        //     <h3 className="d-flex justify-content-center">Login Form</h3>
        //     <form className="form-cls" >
        //         <LoginsComponent/>
        //         <button className="btn btn-danger" type="submit">
        //             Submit
        //         </button>
        //     </form>
        // </div>

        // <div className="container">
        //     <TodoListComponent />
        // </div>
    // );
}



export default App;
